# CTMouse

The FreeDOS mouse driver


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## CTMOUSE.LSM

<table>
<tr><td>title</td><td>CTMouse</td></tr>
<tr><td>version</td><td>2.1b4</td></tr>
<tr><td>entered&nbsp;date</td><td>2008-07-22</td></tr>
<tr><td>description</td><td>The FreeDOS mouse driver</td></tr>
<tr><td>keywords</td><td>Mouse, mice, wheel</td></tr>
<tr><td>author</td><td>Nagy Daniel</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer</td></tr>
<tr><td>primary&nbsp;site</td><td>http://cutemouse.sourceforge.net</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://sourceforge.net/projects/cutemouse</td></tr>
<tr><td>platforms</td><td>FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Mouse</td></tr>
</table>
